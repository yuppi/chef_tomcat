name             'tomcat'
maintainer       'D2C Inc.'
maintainer_email 'uls233@d2c.co.jp'
license          'All rights reserved'
description      'Installs/Configures tomcat'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'ark'
