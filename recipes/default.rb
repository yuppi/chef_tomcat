#
# Cookbook Name:: tomcat
# Recipe:: default
#
# Copyright 2014, D2C Inc.
#
# All rights reserved - Do Not Redistribute
#
ark 'tomcat' do
  url node[:tomcat][:download_url]
  version node[:tomcat][:version]
  prefix_root '/opt'
  prefix_home '/opt'
  owner 'vagrant'
  group 'vagrant'
end
