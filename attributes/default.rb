default[:tomcat][:version] = '8.0.15'
default[:tomcat][:download_url] = "http://ftp.riken.jp/net/apache/tomcat/tomcat-8/v#{node[:tomcat][:version]}/bin/apache-tomcat-#{node[:tomcat][:version]}.tar.gz"
